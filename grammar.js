// Borrowed from
// https://github.com/Quaqqer/tree-sitter-sylt/blob/52959acb374576f7e738257e1cf849878fd22e33/grammar.js

function sep1(rule, separator, trailing) {
    return seq(
        rule,
        repeat(seq(separator, rule)),
        ...(trailing ? [optional(separator)] : [])
    );
}

function sep(rule, separator, trailing) {
    return optional(sep1(rule, separator, trailing));
}

module.exports = grammar({
    name: "spade",

    extras: $ => [/\s/, $.doc_comment, $.line_comment, $.block_comment],

    rules: {
        source_file: $ => repeat($._item),

        attribute: $ => seq(
            '#', '[', $.identifier,
            optional(seq(
                '(',
                sep($.attribute_argument, ',', true),
                ')'
            )),
            ']'
        ),

        attribute_argument: $ => choice(
            $.identifier,
            seq($.identifier, '=', $._expression)
        ),

        _item: $ => seq(
            optional(repeat($.attribute)),
            choice(
                $.unit_definition,
                $.extern_unit_declaration,
                $.struct_definition,
                $.enum_definition,
                $.use,
                $.impl,
                $.trait,
                $.mod
            )
        ),

        unit_definition: $ => seq(
            $._unit_head,
            $._body
        ),

        extern_unit_declaration: $ => seq(
            'extern',
            $._unit_head,
            ';'
        ),

        unit_declaration: $ => seq(
            $._unit_head,
            ';'
        ),

        _unit_head: $ => seq(
            choice(
                'fn',
                'entity',
                $._pipeline_start
            ),
            $.identifier,
            optional($.generic_parameters),
            $.parameter_list,
            optional(
                seq(
                    '->',
                    $.type,
                )
            ),
            optional($.where_clause),
        ),

        _fn_start: $ => 'fn',
        _entity_start: $ => 'entity',
        _pipeline_start: $ => seq(
            'pipeline',
            '(',
            $.type,
            ')',
        ),

        where_clause: $ => seq(
            'where',
            sep(
                seq($.identifier, ':', $._expression),
                ','
            )
        ),

        struct_definition: $ => seq(
            'struct',
            optional('port'),
            $.identifier,
            optional($.generic_parameters),
            $.braced_parameter_list,
        ),


        enum_definition: $ => seq(
            'enum',
            $.identifier,
            optional($.generic_parameters),
            $.enum_body,
        ),

        impl: $ => seq(
            'impl',
            optional($.generic_parameters),
            $._scoped_or_raw_ident,
            optional($.generic_parameters),
            field('for', optional(seq('for', $._scoped_or_raw_ident, optional($.generic_parameters)))),
            '{',
            repeat($.unit_definition),
            '}'
        ),

        trait: $ => seq(
            'trait',
            $._scoped_or_raw_ident,
            optional($.generic_parameters),
            '{',
            repeat($.unit_declaration),
            '}'
        ),

        enum_body: $ => seq('{', sep($.enum_member, ',', true), '}'),

        enum_member: $ => seq($.identifier, optional($.braced_parameter_list)),

        use: $ => seq(
            'use',
            $.scoped_identifier,
            ';'
        ),

        mod: $ => seq(
            'mod',
            $.identifier,
            '{',
            repeat($._item),
            '}'
        ),

        _body: $ => seq(
            $.block
        ),

        block: $ => seq(
            '{',
            repeat($._statement),
            optional($._expression),
            '}'
        ),

        _statement: $ => choice(
            seq($.let_binding, ';'),
            seq($.set_statement, ';'),
            seq($.reg_statement, ';'),
            seq($.pipeline_reg_marker, ';'),
            seq($.assert_statement, ';'),
            seq($.decl_statement, ';'),
            seq($.pipeline_stage_name),
        ),

        let_binding: $ => seq(
            'let',
            $._pattern,
            optional(seq(':', $.type)),
            '=',
            $._expression,
        ),

        reg_statement: $ => seq(
            'reg',
            '(',
            $.identifier,
            ')',
            $._pattern,
            optional(seq(':', $.type)),
            repeat(choice($.reg_reset, $.reg_initial)),
            '=',
            $._expression,
        ),

        set_statement: $ => seq(
            'set',
            $._expression,
            '=',
            $._expression
        ),

        decl_statement: $ => seq('decl', sep1($.identifier, ',', true)),

        pipeline_reg_marker: $ => seq('reg', optional(seq('*', $.type))),
        pipeline_stage_name: $ => seq('\'', $.identifier),

        assert_statement: $ => seq('assert', $._expression),

        reg_reset: $ => seq(
            'reset',
            '(',
            $._expression,
            ':',
            $._expression,
            ')',
        ),

        reg_initial: $ => seq(
            'initial',
            '(',
            $._expression,
            ')',
        ),

        _expression: $ => choice(
            $._base_expression,
            $.binary_expression,
            $.unary_expression,
        ),

        op_add: $ => '+',
        op_sub: $ => '-',
        op_mul: $ => '*',
        op_div: $ => '/',
        op_mod: $ => '%',
        op_equals: $ => '==',
        op_ne: $ => '!=',
        op_lt: $ => '<',
        op_gt: $ => '>',
        op_le: $ => '<=',
        op_ge: $ => '>=',
        op_lshift: $ => '<<',
        op_rshift: $ => '>>',
        op_bitwise_and: $ => '&',
        op_bitwise_xor: $ => '^',
        op_bitwise_or: $ => '|',
        op_logical_and: $ => '&&',
        op_logical_or: $ => '||',

        // Combined parsers for operators which have the same precedence
        _op_mul_like: $ => choice($.op_mul, $.op_div, $.op_mod),
        _op_add_like: $ => choice($.op_add, $.op_sub),
        _op_relational: $ => choice($.op_lt, $.op_gt, $.op_le, $.op_ge),
        _op_eq_like: $ => choice($.op_equals, $.op_ne),
        _op_shifty: $ => choice($.op_lshift, $.op_rshift),
        _op_shifty: $ => choice($.op_lshift, $.op_rshift),

        op_custom_infix: $ => seq('`', $.identifier, '`'),

        binary_expression: $ => choice(
            prec.left(-1, seq($._expression, $._op_mul_like, $._expression)),
            prec.left(-2, seq($._expression, $._op_add_like, $._expression)),
            prec.left(-3, seq($._expression, $._op_shifty, $._expression)),
            prec.left(-4, seq($._expression, $._op_eq_like, $._expression)),
            prec.left(-5, seq($._expression, $._op_relational, $._expression)),
            prec.left(-6, seq($._expression, $.op_bitwise_and, $._expression)),
            prec.left(-7, seq($._expression, $.op_bitwise_xor, $._expression)),
            prec.left(-8, seq($._expression, $.op_bitwise_or, $._expression)),
            prec.left(-9, seq($._expression, $.op_logical_and, $._expression)),
            prec.left(-10, seq($._expression, $.op_logical_or, $._expression)),
            prec.left(-11, seq($._expression, $.op_custom_infix, $._expression)),
        ),

        unary_expression: $ => prec(
            0,
            choice(
                seq($.op_sub, $._expression),
                seq('!', $._expression),
                seq('*', $._expression),
                seq('&', $._expression),
                seq('~', $._expression)
            )
        ),

        if_expression: $ => seq(
            'if',
            $._expression,
            $.block,
            'else',
            choice($.if_expression, $.block)
        ),

        gen_if_expression: $ => seq(
            'gen',
            'if',
            $._expression,
            $.block,
            'else',
            choice($.naked_gen_if_expression, $.gen_if_expression, $.block)
        ),

        naked_gen_if_expression: $ => seq(
            'if',
            $._expression,
            $.block,
            'else',
            choice($.naked_gen_if_expression, $.gen_if_expression, $.block)
        ),

        match_expression: $ => seq(
            'match',
            $._expression,
            $.match_block
        ),

        match_block: $ => seq(
            '{',
            optional(seq(
                repeat($.match_arm),
                alias($.last_match_arm, $.match_arm)
            )),
            '}'
        ),

        match_arm: $ => seq(
            $._pattern,
            '=>',
            choice(
                seq($._expression, ','),
            )
        ),

        last_match_arm: $ => seq(
            field('pattern', $._pattern),
            '=>',
            field('value', $._expression),
            optional(',')
        ),

        _base_expression: $ => choice(
            $._simple_base_expression,
        ),


        _simple_base_expression: $ => choice(
            $.bool_literal,
            $.int_literal,
            $.array_literal,
            $.tuple_literal,
            $._scoped_or_raw_ident,
            $.match_expression,
            $.function_call,
            $.entity_instance,
            $.pipeline_instance,
            $.stage_reference,
            $.if_expression,
            $.gen_if_expression,
            $.paren_expression,
            $.field_access,
            $.method_call,
            $.index,
            $.block,
            $.self
        ),

        field_access: $ => seq($._simple_base_expression, '.', $.identifier),
        method_call: $ => seq(
            field('base', $._simple_base_expression),
            '.',
            optional('inst'),
            field('name', $.identifier),
            optional($.turbofish),
            $.argument_list
        ),
        index: $ => seq(
            $._simple_base_expression,
            "[",
            $._expression,
            "]"
        ),

        array_literal: $ => seq('[', sep($._expression, ',', true), ']'),
        tuple_literal: $ => seq('(', $._expression, ',', sep($._expression, ',', true), ')'),

        paren_expression: $ => seq('(', $._expression, ')'),

        function_call: $ => seq($._scoped_or_raw_ident, optional($.turbofish), $.argument_list),
        entity_instance: $ => seq('inst', $._scoped_or_raw_ident, optional($.turbofish), $.argument_list),
        pipeline_instance: $ => seq('inst', '(', $.type, ')', optional($.turbofish), $._scoped_or_raw_ident, $.argument_list),

        turbofish: $ => choice(
            seq('::<', sep($.type, ',', true), '>'),
            seq('::$<', sep($.named_turbofish_arg, ',', true), '>')
        ),

        named_turbofish_arg: $ => choice($.identifier, seq($.identifier, ':', $.type)),

        stage_reference: $ => seq(
            'stage',
            '(',
            choice(field('stage', $.identifier), seq(choice('-', '+'), $.type)),
            ').',
            $.identifier
        ),

        bool_literal: $ => choice(
            'true',
            'false'
        ),

        argument_list: $ => choice(
            $._positional_argument_list,
            $._named_argument_list
        ),

        _named_argument_list: $ => seq(
            '$(',
            sep($._named_argument, ',', true),
            ')'
        ),

        _named_argument: $ => choice(
            $.parameter,
            seq($.parameter, ':', $._expression)
        ),

        _positional_argument_list: $ => seq(
            '(',
            sep(seq($._expression), ',', true),
            ')'
        ),

        parameter_list: $ => seq(
            '(',
            optional(seq($.self, optional(','))),
            sep($.typed_parameter, ',', true),
            ')',
        ),

        braced_parameter_list: $ => seq(
            '{',
            sep($.typed_parameter, ',', true),
            '}',
        ),

        typed_parameter: $ => seq(
            optional(repeat($.attribute)),
            $.parameter,
            ':',
            $.type
        ),

        parameter: $ => $.identifier,
        self: $ => 'self',

        _pattern: $ => choice(
            $._scoped_or_raw_ident,
            $.int_literal,
            $.bool_literal,
            $.tuple_pattern,
            $.named_unpack,
            $.positional_unpack
        ),

        tuple_pattern: $ => seq('(', sep($._pattern, ',', true), ')'),
        named_unpack: $ => seq($._scoped_or_raw_ident, $._named_pattern_list),
        positional_unpack: $ => seq($._scoped_or_raw_ident, $._positional_pattern_list),

        _named_pattern_list: $ => seq('$(', sep($.named_pattern_param, ',', true), ')'),
        _positional_pattern_list: $ => seq('(', sep($._pattern, ',', true), ')'),

        named_pattern_param: $ => choice(
            $.parameter,
            seq($.parameter, ':', $._pattern)
        ),

        type: $ => choice(
            // Base type with generics
            seq(
                $._base_type,
                optional($._generic_list),
            ),
            $.tuple_type,
            $.array_type,
            $.wire,
            $.inverted,
            seq('{', $._expression, '}')
        ),

        tuple_type: $ => seq('(', sep($.type, ',', true), ')'),
        array_type: $ => seq('[', $.type, ';', $.type, ']'),
        wire: $ => seq('&', $.type),
        inverted: $ => seq('inv', $.type),

        _generic_list: $ => seq(
            '<',
            sep($.type, ',', true),
            '>'
        ),

        generic_parameters: $ => seq(
            '<',
            sep($.generic_param, ',', true),
            '>'
        ),

        generic_param: $ => seq(
            field('meta', optional(seq('#', $.identifier))),
            $.identifier
        ),

        _base_type: $ => choice(
            $.builtin_type,
            $.identifier,
            $.int_literal
        ),

        builtin_type: $ => choice(
            'int',
            'bool',
            'clock'
        ),

        // TODO: Same regex as spade
        identifier: $ => /[\p{XID_Start}_]\p{XID_Continue}*/u,

        scoped_identifier: $ => seq(
            field('path', optional(choice(
                $._path,
            ))),
            '::',
            field('name', $.identifier)
        ),

        _path: $ => choice($.scoped_identifier, $.identifier),
        _scoped_or_raw_ident: $ => choice($.scoped_identifier, $.identifier),

        int_literal: $ => choice(
            /[0-9][0-9_]*([ui][0-9]+)?/,
            /0x[0-9A-Fa-f][0-9_A-Fa-f]*([ui][0-9]+)?/,
            /0b[0-1][0-1_]*([ui][0-9]+)?/
        ),

        doc_comment: $ => token(seq(
            '///', /.*/
        )),

        comment: $ => choice(
            $.line_comment,
            $.block_comment,
        ),

        line_comment: $ => token(seq(
            '//', /.*/
        )),

        // https://github.com/tree-sitter/tree-sitter/discussions/2094#discussioncomment-5082953
        block_comment: $ => seq(
            "/*",
            repeat(choice(/.|\n|\r/)),
            "*/"
        ),
    }
})
