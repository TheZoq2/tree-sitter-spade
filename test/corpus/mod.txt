===
Simple mod
===
mod a {
    use a::b;
}
---

(source_file
  (mod
    (identifier)
    (use
      (scoped_identifier
        (identifier)
        (identifier)))))


===
Empty mod
===
mod a {
}
---

(source_file
  (mod
    (identifier)))
