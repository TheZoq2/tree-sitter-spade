================
Reg statements
================

fn x() {
  reg(clk) x = 0;
  true
}

fn x() {
  reg(clk) x reset(rst: true) = 0;
  true
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (reg_statement
        (identifier)
        (identifier)
        (int_literal))
        (bool_literal)))
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (reg_statement
        (identifier)
        (identifier)
        (reg_reset
            (identifier)
            (bool_literal))
        (int_literal))
        (bool_literal)))
)


================
With type signature
================
fn x() {
  reg(clk) x: bool = 0;
  true
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (reg_statement
        (identifier)
        (identifier)
        (type (builtin_type))
        (int_literal))
      (bool_literal))))

        
===
Reg regression
===

fn a() {
  reg(clk) state reset(rst: LoweringState::Idle) = true;
}

---
(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (reg_statement
        (identifier)
        (identifier)
        (reg_reset
          (identifier)
          (scoped_identifier (identifier) (identifier)))
        (bool_literal)))))

        
===
Reg initial
===

fn a() {
  reg(clk) state initial(LoweringState::Idle) = true;
}

---
(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (reg_statement
        (identifier)
        (identifier)
        (reg_initial
          (scoped_identifier (identifier) (identifier)))
        (bool_literal)))))
